== Description ==
This plugin allows you to modify the current packages or upload texts/videos.

== Installation ==
1. Copy this directory into your `wp-content/plugins` directory.
2. Modify the constants in `main.js` if necessary.
3. Enable the plugin in the Wordpress Dashboard.

== Usage ==
Create a new page which uses the shortcode `[regiotv-editor]` or `[regiotv-generator-video]`/`[regiotv-generator-message]`.
test
