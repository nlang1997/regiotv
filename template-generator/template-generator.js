//Controls erfassen

window.onload = function () {

var changeTemplateNameButton = document.getElementById("changeTemplateNameButton");
var deleteTemplateButton = document.getElementById("deleteTemplateButton");


var templateNameText = document.getElementById("templateNameText");

var saveTemplateBtn = document.getElementById("saveTemplateBtn");
//die Buttons deklarieren
var createNewTemplateButton = document.getElementById("createNewTemplateButton");
var editorContainer = document.getElementById("editorContainer");
var editorDragDrop = document.getElementById("editorDragDropContainer");
var addTitleBtn = document.getElementById("addTitleFieldBtn");
var addBoxBtn = document.getElementById("addBoxButton");
var addAuthorBtn = document.getElementById("addAuthorFieldButton");
var addSourcesBtn = document.getElementById("addSourceFieldButton");
var addTextFieldName = document.getElementById("addTextFieldName");
var addTextFieldBtnAdd = document.getElementById("addTextFieldBtnAdd");
var templateSelector = document.getElementById("templateSelector");
var templateMenu = document.getElementById("templateMenu");
var templateCreatorDiv = document.getElementById("templateCreatorDiv");

//letztes bewegtes element
var lastMovedElement;
//selektiertesTemplate;
var selectedTemplate;


//den Buttons EventListener hinzufügen
addTextFieldBtnAdd.addEventListener("click", addTextField);
addTitleBtn.addEventListener("click", addTitle);
addBoxBtn.addEventListener("click", addBox);
addAuthorBtn.addEventListener("click", addAuthors);
addSourcesBtn.addEventListener("click", addSources);
createNewTemplateButton.addEventListener("click", openTemplateCreator);
saveTemplateBtn.addEventListener("click", createTemplate);
templateNameText.addEventListener("keyup", function() {
  this.classList.remove("inputRequiredField");
});

function createOrEditTemplate() {

}

function TextBeautifier(elmn) {

  let oldBeautifier = document.getElementById("beautfier");

  if (document.body.contains(oldBeautifier)) {
    oldBeautifier.parentNode.removeChild(oldBeautifier);
  }

  var childNodeP;
  let s = elmn;
    for(var i = 0; i < s.childNodes.length; i++) {

        let iterator = s.childNodes[i];

        if (iterator.nodeName == "P") {
          childNodeP = iterator;


          }
      }

  if (childNodeP.id == "blackBox") {
    return;
  }


  let div = document.createElement("div");


  let availOpt = [8, 9 ,12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72];

  var select = document.createElement("SELECT");



  select.addEventListener("change", function() {
      childNodeP.style.fontSize = this.value + "px";
      elmn.style.fontSize = this.value + "px";
    addCurrentPositionPointers(s);
  });

  for(var i = 0; i < availOpt.length; i++) {
    let iavailOpt = availOpt[i];
    var z = document.createElement("option");
    z.setAttribute("value", iavailOpt);
    var t = document.createTextNode(iavailOpt);
    z.appendChild(t);
    select.appendChild(z);
  }


  select.value = childNodeP.style.fontSize.replace("px", "");

  div.appendChild(select);


  div.id = "beautfier";
  div.style.left = elmn.style.left;
  div.style.top = (elmn.offsetTop - 32) + "px";
  editorDragDrop.appendChild(div);


}

initTemplateContainerOnClick();
//Gebe allen Templates die Möglichkeit markiert zu werden
function initTemplateContainerOnClick() {
  let allTemplates = document.getElementsByClassName("templateContainer");

  for(var i = 0; i < allTemplates.length; i++) {
    allTemplates[i].addEventListener("click", function() {
      if(this.classList.contains("selectedTemplate")) {
        this.classList.remove("selectedTemplate");
        let checkb = document.getElementById("checkbox" + this.id);
        checkb.checked = false;
        changeTemplateNameButton.disabled = true;
        deleteTemplateButton.disabled = true;
        createNewTemplateButton.innerHTML = "Erstellen";

        templateCreatorDiv.classList.remove("minimizeCreatorDiv");
      } else {
          for(var s = 0; s < allTemplates.length; s++) {
            allTemplates[s].classList.remove("selectedTemplate");
            let checkb = document.getElementById("checkbox" + allTemplates[s].id);
            checkb.checked = false;
          }

          this.classList.add("selectedTemplate");
          let checkb = document.getElementById("checkbox" + this.id);
          checkb.checked = true;
          selectedTemplate = this;
          templateCreatorDiv.classList.add("minimizeCreatorDiv");
          changeTemplateNameButton.disabled = false;
          deleteTemplateButton.disabled = false;
          createNewTemplateButton.innerHTML = "Bearbeiten";


      }

    });
  }
}



function removeLastMovedElementMark() {
  //entfernt die Markierung des zuletzt bewegten Elements
  if (document.body.contains(lastMovedElement)) {
    lastMovedElement.classList.remove("lastMovedElement");
    let currentPositionPointer =  document.getElementById("currentPositionPointer");
    let textBeautifier = document.getElementById("beautfier");

    if (document.body.contains(currentPositionPointer)) {
      currentPositionPointer.parentNode.removeChild(currentPositionPointer);
    }

    if (document.body.contains(textBeautifier)) {
      textBeautifier.parentNode.removeChild(textBeautifier);
    }
  }
}

function openTemplateCreator() {
  if (templateNameText.value.length == 0) {
    alert("Bitte geben Sie der Vorlage einen Namen!");
    templateNameText.classList.add("inputRequiredField");
    return;
  }

  if(editorContainer.classList.contains("editorContainerVisible")) {
    editorContainer.classList.remove("editorContainerVisible");
    templateMenu.classList.remove("templateSelectorHidden");


  } else {
    editorContainer.classList.add("editorContainerVisible");
    templateMenu.classList.add("templateSelectorHidden");
  document.getElementById("templateTitleLabel").innerHTML = templateNameText.value;
  }
}

function addCurrentPositionPointers(elmnt) {

    let oldPositionPointer = document.getElementById("currentPositionPointer");

    if (document.body.contains(oldPositionPointer)) {
      oldPositionPointer.parentNode.removeChild(oldPositionPointer);
    }

    let positionPointerDiv = document.createElement("div");
    positionPointerDiv.id = "currentPositionPointer";
    positionPointerDiv.style.position = "absolute";


    positionPointerDiv.style.left = elmnt.style.left;
    /*positionPointerDiv.style.left = elmnt.offsetLeft;*/
    /*positionPointerDiv.style.top = (parseInt(elmnt.style.top, 10) + parseInt(elmnt.style.height, 10)) + "px";*/
    /*positionPointerDiv.style.top = (parseInt(elmnt.style.top, 10) + elmnt.offsetHeight) + "px";*/
    positionPointerDiv.style.top = (elmnt.offsetTop + elmnt.offsetHeight) + "px"

    let posXLbl = document.createElement("LABEL");
    posXLbl.innerHTML = "X:";
    let posYLbl = document.createElement("LABEL");
    posYLbl.innerHTML = "Y:";

    let positionX = document.createElement("INPUT");
    positionX.setAttribute("type", "text");
    positionX.setAttribute("maxlength", "3");

    let positionY = document.createElement("INPUT");
    positionY.setAttribute("type", "text");
    positionY.setAttribute("maxlength", "3");

    positionX.value = elmnt.offsetLeft;
    positionY.value = elmnt.offsetTop;

    positionX.addEventListener('input', function (evt) {
      elmnt.style.left = this.value + "px";
      positionPointerDiv.style.left = elmnt.style.left;
      TextBeautifier(elmnt);
    });

    positionY.addEventListener('input', function (evt) {
      elmnt.style.top = this.value + "px";
      positionPointerDiv.style.top = (parseInt(elmnt.style.top, 10) + elmnt.offsetHeight) + "px";
      TextBeautifier(elmnt);
    });

    posXLbl.appendChild(positionX);
    posYLbl.appendChild(positionY);


    positionPointerDiv.appendChild(posXLbl);
    positionPointerDiv.appendChild(posYLbl);


    editorDragDrop.appendChild(positionPointerDiv);

}

function addAuthors() {

  if (document.contains(document.getElementById("authorsBox"))) {
    return ;
  }

  var newPopupLbl = document.createElement("p");
  newPopupLbl.innerHTML = "Autoren";
  var id = "authorsBox";
  var capsel = capselResizableDiv(id, newPopupLbl);
  editorDragDrop.appendChild(capsel);
  initDragElement();
    initResizeElement();
}

function addSources() {

  if (document.contains(document.getElementById("sourcesBox"))) {
    return ;
  }

  var newPopupLbl = document.createElement("p");
  newPopupLbl.innerHTML = "Quellen";
  var id = "sourcesBox";
  var capsel = capselResizableDiv(id, newPopupLbl);
  editorDragDrop.appendChild(capsel);
  initDragElement();
  initResizeElement();
}


function addBox() {

  if (document.contains(document.getElementById("blackBox"))) {
    return ;
  }

  var newPopupLbl = document.createElement("p");
  newPopupLbl.id = "blackBox";
  var capsel = capselResizableDiv(0, newPopupLbl);
  capsel.classList.add("noincrease");
  capsel.style.width = "100px";
  capsel.style.height = "50px";
  editorDragDrop.appendChild(capsel);

  initDragElement();
  initResizeElement();
}

function addTitle() {

  if (document.contains(document.getElementById("titleBox"))) {
    return ;
  }

  var newPopupLbl = document.createElement("p");
  newPopupLbl.innerHTML = "Titel";
  var id = "titleBox";
  var capsel = capselResizableDiv(id, newPopupLbl);
  editorDragDrop.appendChild(capsel);
  initDragElement();


}

function addTextField() {

  if ( addTextFieldName.value == "" ) {
    return;
  }
  let placeholderText = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";

  var newPopupLbl = document.createElement("p");
  newPopupLbl.className = "tagField";
  newPopupLbl.id = "textFieldIdentificator_" + addTextFieldName.value;
  newPopupLbl.innerHTML = placeholderText;

  var capsel = capselTextField(addTextFieldName.value, newPopupLbl);



  editorDragDrop.appendChild(capsel);
  addTextFieldName.value = "";
  initDragElement();
  initResizeElement();
}

function removeResizeElement(elementid) {
  console.log("hello");
  var element = document.getElementById(elementid);
  if (!document.body.contains(element)) {
    return null
  }
  for(i = 0;  i < element.childNodes.length; i++) {
    if(element.childNodes[i].classList.contains("resizer")) {
      console.log("ja");
      element.removeChild(element.childNodes[i]);
    } else {
      console.log("nein");
    }
  }
}


function createTemplate() {
  let titleBox = document.getElementById("titleBox");
  let authorsBox = document.getElementById("authorsBox");
  let sourcesBox = document.getElementById("sourcesBox");
  let blackBox = document.getElementById("blackBox");
  let tagFields = document.getElementsByClassName("tagField");


  let template;

  let title_settings;
  let tags = [];
  let authors_settings;
  let sources_settings;
  let box_settings;


  //Titelbox
  if (document.body.contains(titleBox)) {
    title_settings = new SubSetting("title", true, titleBox.offsetLeft, titleBox.offsetTop, titleBox.offsetWidth, titleBox.offsetHeight, titleBox.style.fontSize);

  } else {
    title_settings = new SubSetting("title", false, 0, 0, 0, 0, "10", "Arial", "#000000", "left", "bold");
  }

  if (tagFields.length > 0) {
    for(var i = 0; i < tagFields.length; i++) {
        let iTag = tagFields[i];
        let name = iTag.id.replace("textFieldIdentificator_", "");
        tags.push(new SubSetting(name, true, iTag.parentNode.offsetLeft, iTag.parentNode.offsetTop, iTag.parentNode.offsetWidth, iTag.parentNode.offsetHeight, iTag.style.fontSize));
    }
  }

  //authorsBox
  if (document.body.contains(authorsBox)) {
    authors_settings = new SubSetting("authors", true, authorsBox.offsetLeft, authorsBox.offsetTop, authorsBox.offsetWidth, authorsBox.offsetHeight, authorsBox.style.fontSize);

  } else {
    authors_settings = new SubSetting("authors", false, 0, 0, 0, 0, "10px");
  }

  //sourcesBox
  if (document.body.contains(sourcesBox)) {
    sources_settings = new SubSetting("sources", true, sourcesBox.offsetLeft, sourcesBox.offsetTop, sourcesBox.offsetWidth, sourcesBox.offsetHeight, sourcesBox.style.fontSize);

  } else {
    sources_settings = new SubSetting("sources", false, 0, 0, 0, 0, "10px");
  }

  //blackBox
  if (document.body.contains(blackBox)) {
    box_settings = new SubSetting("sources", true, blackBox.offsetLeft, blackBox.offsetTop, blackBox.offsetWidth, blackBox.offsetHeight, blackBox.style.fontSize);

  } else {
    box_settings = new SubSetting("sources", false, 0, 0, 0, 0, "10px");
  }

  //Template

  template = new Template(templateNameText.value, title_settings, tags, box_settings, sources_settings, authors_settings );
  console.log(JSON.stringify(template));
  showOutputCode(JSON.stringify(template, null, "\t"));
}

function showOutputCode(code) {
  let textarea = document.getElementById("codeOutput");

  textarea.style.display = "block";
  textarea.innerHTML = code;
}

class Template {

  constructor(title, title_settings, tags, box_settings, sources_settings, authors_settings) {
    this.id = 0;
    this.title = title;
    this.wp_author_id = 0;
    this.title_settings = title_settings;
    this.tags = tags;
    this.box_settings = box_settings;
    this.sources_settings = sources_settings;
    this.authors_settings = authors_settings;
    this.thumbnail = "background.jpg";
  }
}

class SubSetting {

  constructor(name, enabled, x, y, width, height, font_size) {
    this.name = name;
    this.enabled = enabled;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.font_size = font_size;
  }

}

class Thumbnail {
  constructor(image) {
    this.image = image;
  }
}

function capselResizableDiv(id, element) {

  var popupDiv = document.createElement("div");
  if (id != 0) {
    popupDiv.id = id;
  }
  popupDiv.classList.add("popup");
  var newPopupHeaderDiv = document.createElement("div");
  newPopupHeaderDiv.classList.add("popup-header", "noselect");

  /*removeButton*/
  var rmB = document.createElement("div");
  rmB.onclick = function(e) {
     this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
       let oldPositionPointer = document.getElementById("currentPositionPointer");
       let oldBeautifier = document.getElementById("beautfier");

     if (document.body.contains(oldPositionPointer)) {
       oldPositionPointer.parentNode.removeChild(oldPositionPointer);
     }

     if (document.body.contains(oldBeautifier)) {
       oldBeautifier.parentNode.removeChild(oldBeautifier);
     }
   };
  rmB.className = "removeButton";
  var rmBp = document.createElement("p");
  rmBp.innerHTML = "x";
  rmB.appendChild(rmBp);

  newPopupHeaderDiv.appendChild(rmB);


  popupDiv.appendChild(newPopupHeaderDiv);
  popupDiv.appendChild(element);
  return popupDiv;
}

function capselTextField(fieldname, element) {
  var popupDiv = document.createElement("div");

  popupDiv.classList.add("popup");
  var newPopupHeaderDiv = document.createElement("div");
  newPopupHeaderDiv.classList.add("popup-header", "noselect");


  //das muss noch hinzugefügt werden
  var itemName = document.createElement("p");
  itemName.className = "tagName";
  itemName.innerHTML = fieldname;
  /*removeButton*/
  var rmB = document.createElement("div");
  rmB.onclick = function(e) {
     this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
       let oldPositionPointer = document.getElementById("currentPositionPointer");
       let oldBeautifier = document.getElementById("beautfier");

     if (document.body.contains(oldPositionPointer)) {
       oldPositionPointer.parentNode.removeChild(oldPositionPointer);
     }

     if (document.body.contains(oldBeautifier)) {
       oldBeautifier.parentNode.removeChild(oldBeautifier);
     }
   };
  rmB.className = "removeButton";
  var rmBp = document.createElement("p");
  rmBp.innerHTML = "x";
  rmB.appendChild(rmBp);

  newPopupHeaderDiv.appendChild(rmB);
  newPopupHeaderDiv.appendChild(itemName);

  popupDiv.appendChild(newPopupHeaderDiv);
  popupDiv.appendChild(element);
  return popupDiv;

}

function initDragElement() {
  var pos1 = 0,
    pos2 = 0,
    pos3 = 0,
    pos4 = 0;
  var popups = document.getElementsByClassName("popup");
  var elmnt = null;
  var currentZIndex = 100; //TODO reset z index when a threshold is passed

  for (var i = 0; i < popups.length; i++) {
    var popup = popups[i];
    var header = getHeader(popup);



    popup.onmousedown = function() {
      this.style.zIndex = "" + ++currentZIndex;
    };

    if (header) {
      header.parentPopup = popup;
      header.onmousedown = dragMouseDown;
    }
  }

  function dragMouseDown(e) {
    elmnt = this.parentPopup;
  /*  elmnt.style.zIndex = "" + ++currentZIndex;*/
   elmnt.style.zIndex = "" + ++currentZIndex;


    e = e || window.event;
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;

    //wenn ein Element angeklickt wird, wird des als zuletzt bewegtes Element markiert
    markLastMovedElement(elmnt);

  }

  function markLastMovedElement(elmnt) {
    if(lastMovedElement) {
      lastMovedElement.classList.remove("lastMovedElement");
    }

    if (elmnt) {
      lastMovedElement = elmnt;
      lastMovedElement.classList.add("lastMovedElement");
      addCurrentPositionPointers(elmnt);
      TextBeautifier(elmnt);
    }
  }

  function elementDrag(e) {
    if (!elmnt) {
      return;
    }


    e = e || window.event;
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    var elmtop = elmnt.style.top.split("px")[0];

    if (elmtop >= 0 && elmtop <= 360) {
        elmnt.style.top = elmnt.offsetTop - pos2 + "px";
    }else if (elmtop < 0) {
      elmnt.style.top = "0px";
    }else if (elmtop > 360) {
      elmnt.style.top = "360px";
    }

    var elmleft = elmnt.style.left.split("px")[0];
    if (elmleft >= 0 && elmleft <= 640) {
      elmnt.style.left = elmnt.offsetLeft - pos1 + "px";
    }else if (elmleft < 0) {
      elmnt.style.left = "0px";
    }else if (elmleft > 640) {
      elmnt.style.left = "640px";
    }

    addCurrentPositionPointers(elmnt);
    TextBeautifier(elmnt);
  }

  function closeDragElement() {
    /* stop moving when mouse button is released:*/
    document.onmouseup = null;
    document.onmousemove = null;

  }



  function getHeader(element) {
    var headerItems = element.getElementsByClassName("popup-header");

    if (headerItems.length === 1) {
      return headerItems[0];
    }

    return null;
  }
}

function initResizeElement() {
  var popups = document.getElementsByClassName("popup");
  var element = null;
  var startX, startY, startWidth, startHeight;

  for (var i = 0; i < popups.length; i++) {
    var p = popups[i];

    var right = document.createElement("div");
    right.className = "resizer-right";
    right.classList.add("resizer");
    p.appendChild(right);
    right.addEventListener("mousedown", initDrag, false);
    right.parentPopup = p;

    var bottom = document.createElement("div");
    bottom.className = "resizer-bottom";
    bottom.classList.add("resizer");
    p.appendChild(bottom);
    bottom.addEventListener("mousedown", initDrag, false);
    bottom.parentPopup = p;

    var both = document.createElement("div");
    both.classList.add("resizer");
    both.className = "resizer-both";
    p.appendChild(both);
    both.addEventListener("mousedown", initDrag, false);
    both.parentPopup = p;
  }

    removeResizeElement("titleBox");

  function initDrag(e) {
    element = this.parentPopup;

    startX = e.clientX;
    startY = e.clientY;
    startWidth = parseInt(
      document.defaultView.getComputedStyle(element).width,
      10
    );
    startHeight = parseInt(
      document.defaultView.getComputedStyle(element).height,
      10
    );
    document.documentElement.addEventListener("mousemove", doDrag, false);
    document.documentElement.addEventListener("mouseup", stopDrag, false);
  }

  function doDrag(e) {
    element.style.width = startWidth + e.clientX - startX + "px";
    element.style.height = startHeight + e.clientY - startY + "px";
    addCurrentPositionPointers(element);
    TextBeautifier(element);

  }

  function stopDrag() {
    document.documentElement.removeEventListener("mousemove", doDrag, false);
    document.documentElement.removeEventListener("mouseup", stopDrag, false);

  }


}




};
