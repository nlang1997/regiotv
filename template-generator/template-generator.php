<div id="templateMenu">
<div id="templateSelector">


  <div class="scrollableSelector">
  <?php
    $allTemplates = DesignTemplate::getAllDesignTemplates();

    foreach ( $allTemplates as $template ) {

        echo  '<div id="' . $template->id . '" title="' . $template->title . '" class="templateContainer">';
        echo '<img src="'. $template->background . '"/>';
        echo '<p>'. $template->title . '</p>';
        echo '</div>';
        echo '<input class="hiddenFormular themeselection" type="checkbox" id="checkbox'. $template->id. '" name="'. $template->id .'"/>';
    }

   ?>
</div>
</div>
<div id="templateCreatorDiv">
  <p>Neue Vorlage erstellen</p>
  <input type="text" placeholder="Wie soll Ihre Vorlage heißen?" id="templateNameText">


</div>
<div id="templateCreatorButtonDiv">
  <button id="changeTemplateNameButton" disabled>Umbenennen</button>
  <button id="deleteTemplateButton" disabled>Löschen</button>
  <button id="createNewTemplateButton">Erstellen</button>
</div>
</div>


<div id="editorContainer">

  <h3 id="templateTitleLabel">Default</h3>

  <div id="editorDragDropContainer">

  </div>
  <div id="editorButtonsContainer">
    <div id="addTextFieldContainer">
      <p id="addTextFieldLbl">Textfeld hinzufügen</p>
      <input type="text" id="addTextFieldName"/>
      <button id="addTextFieldBtnAdd">+</button>
    </div>

    <button id="addTitleFieldBtn">Titelfeld hinzufügen</button>
    <button id="addBoxButton">Bereich einfügen</button>
    <button id="addAuthorFieldButton">Autorenfeld hinzufügen</button>
    <button id="addSourceFieldButton">Quellenfeld hinzufügen</button>
    <!--<input type="submit" name="submitTemplate" value="Vorlage speichern"/>-->
    <button id="saveTemplateBtn" >Vorlage speichern</button>
  </div>


</div>
  <textarea cols="100" rows="100" id="codeOutput"></textarea>
<?php



class DesignTemplate {

  public $path;
  public $id;
  public $background;
  public $title;
  public $description;
  public $wp_author_id;
  public $templateContent;

  public function __construct($path, $id, $background, $title, $description, $wp_author_id, $templateContent) {
    $this->path = $path;
    $this->id = $id;
    $this->background = plugin_dir_url( __DIR__ ). 'DesignTemplates/'. $path . "/" . $background;
    $this->title = $title;
    $this->description = $description;
    $this->wp_author_id = $wp_author_id;
    $this->templateContent = $templateContent;
  }

  public static function getAllDesignTemplates() {
    $directory = plugin_dir_path(__DIR__) . "DesignTemplates";
    $templateDirectories = scandir($directory);
    $templateDirectories = array_diff($templateDirectories, array('.', '..'));

    $allTemplates = array();

    foreach ($templateDirectories as $tDir) {
      $path = $directory . "/" . $tDir;

      if (is_dir($path)) {

        if(self::isDirectoryDesignTemplate($path)) {

          $dtp = self::createDesignTemplateByPath($path, $tDir);
          $allTemplates[] = $dtp;
        }

      }


    }
    return $allTemplates;
  }

  public static function createDesignTemplateByPath($path, $dir) {
    $background = "";
    $path = $path . "/style.json";

    if (!file_exists($path)) {
      return null;
    }
    $json = json_decode(file_get_contents($path));

    //print_r($json->title);

    $newDesignTemplate = new DesignTemplate($dir, $json->id, $json->thumbnail, $json->title, $json->description, $json->wp_author_id, $json);

    /*if (file_exists($path . "/background.jpg")) {
      $background = plugin_dir_url(__DIR__) . "DesignTemplates/". $dir . "/background.jpg";
    }*/

    //$dtp = new DesignTemplate("", $json->title, $json->description, 0, $json);

  return $newDesignTemplate;


  }

  public static function isDirectoryDesignTemplate($directory) {
    //Wenn das angegebene Verzeichnis die Datei style.xml enthält
    //gibt diese Funktion True zurück
    //sonst False
    if (file_exists($directory . "/style.json")) {
      return true;
    }

    return false;
  }


}


?>
