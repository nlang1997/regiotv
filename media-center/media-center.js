$(()=>{
    videojs.options.hls.overrideNative = true
    var player = videojs($('video-js').get(0), {
        inactivityTimeout: 0,
        controls:true,
        muted: false,
        autoplay: false,
        preload: "auto",
        fluid:true
    });

    let videolist = $('.video-list');

    $.ajax({
        url:`${REGIOTV_SERVER}/video/list`,
        dataType:"json",
        success:(videoIds)=>{
            for(let videoId of videoIds){
                $.ajax({
                    url:`${REGIOTV_SERVER}/video/metadata?videoid=${videoId}`,
                    method:'GET',
                    dataType:'json',
                    success:(metadata)=>{
                        if(metadata.tags && metadata.tags.includes(category)){
                            var regex = new RegExp("^[0-9]*(.*)V[0-9][0-9][0-9]$")
                            var parsedVideoId = regex.exec(videoId)[1];
                            let listentry = $(`<li  class="list-group-item list-group-item-action col-6" data-id="${videoId}" data-toggle="modal" data-target="#videojsModal">${parsedVideoId}</li>`); 
                            listentry.on('click',(event)=>{
                                $('#videojsModalLabel').text(metadata.title);
                                player.src({src:`${REGIOTV_SERVER}/video/masterplaylist?videoid=${$(event.currentTarget).attr('data-id')}`,type:'application/x-mpegURL'});
                            })
                            videolist.append(listentry);
                        }
                    }
                });
            };
        }
    });
    $('#videojsModal').on('hidden.bs.modal',()=>{
        player.pause();
    });


});
