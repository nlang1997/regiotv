<script> 
    var category = "<?php echo $atts['category']; ?>";
</script>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Datum von</span>
                </div>
                <input type="text" class="form-control" data-name="datefrom">
            </div>
        </div>
        <div class="col">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Datum von</span>
                </div>
                <input type="text" class="form-control" data-name="datefrom">
            </div>
        </div>
        <div class="col">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Datum von</span>
                </div>
                <input type="text" class="form-control" data-name="datefrom">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="btn-group-toggle" data-toggle="buttons">
                <label class="btn btn-secondary active">
                    <input type="checkbox" checked autocomplete="off">Nachrichten 
                </label>
            </div>
        </div>
        <div class="col">
            <div class="btn-group-toggle" data-toggle="buttons">
                <label class="btn btn-secondary">
                    <input type="checkbox" checked autocomplete="off">Videos 
                </label>
            </div>
        </div>
    </div>
        <ul class="video-list list row mx-0 my-3">
        </ul>
    <div class="modal fade" id="videojsModal" tabindex="-1" role="dialog" aria-labelledby="videojsModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title mx-auto pl-5" id="videojsModalLabel">Modal title</h5>
            <button type="button" class="close ml-0" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body"><video-js id='videojsplayer' class='video-js'></video-js>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
</div>
