jQuery(document).ready(($) => {
    // call to origin for obtaining the username of the logged
    // in user
    var userLogin;
    let videoListSave;
    $.ajax({
        type: 'GET',
        beforeSend: function () { },
        url: document.location,
        success: function (data,status,jqHXR) {
            userLogin = jqHXR.getResponseHeader('Userlogin');
            getVideos();
        },
    })

    //API call to get the package titles and display them in the package column
    var getPackages = function(){
    $('#regiotv-packages').empty();
    $.ajax({
        type: 'GET',
        beforeSend: function () { },
        url: REGIOTV_SERVER + '/package/list',
        success: function (data) {
            $.each(data, function (key, val) {
                let cssClass = 'list-group-item ui-state-default'
                var regex = new RegExp("^[0-9]*(.*)V010$")
                var matches = regex.exec(val);
                $('#regiotv-packages').append(`<li title='${val}' class='${cssClass}' id='${val}'>${matches[1]}</li>`)
            })
        }
    })
    }
    getPackages();
    
    //API call to get the video titles and display them in the video column
    let getVideos = function(){
    $.ajax({
        type: 'GET',
        beforeSend: function () { },
        dataType: 'json',
        url: REGIOTV_SERVER + '/video/list',
        success: function (videoList) {
            $.each(videoList, function (index, video) {
                let cssClass = 'list-group-item ui-state-default'
                var regex = new RegExp("^[0-9]*(.*)V010$")
                var matches = regex.exec(video);
                $('#regiotv-videos').prepend(`<p title='${video}' class='${cssClass}' id='${video}'>${matches[1]}</p>`)
            })
            videoListSave = $('#regiotv-videos p');
        } 
      })
    }

    $('#regiotv-package-content, #regiotv-videos').sortable({
        connectWith: '.connectedSortable'
    }).disableSelection()

    // show contained items on click
    $('#regiotv-packages').selectable({
        selected: function (event, ui) {
            $(ui.selected).addClass('active').siblings().removeClass('active')
            $('#regiotv-videos').empty().append(videoListSave);
            const id = $(ui.selected).attr('id')
            $.ajax({
                type: 'GET',
                url: REGIOTV_SERVER + '/package/assembly',
                data: {packageid: id.toString()},
                dataType:'json',
                success: function (packageAssembly) {
                    $.each(packageAssembly.videos, function (_, val) {
                        $('#regiotv-videos p').each(function (_, element) {
                            if ($(element).attr('id') === val.videoid) {
                                $('#regiotv-package-content').append($(element))
                            }
                        })
                    })
                },
                complete: function () {
                    $('#regiotv-packege-content, #regiotv-videos').sortable('refresh')
                }
            })
        }
    })

    $('#addPackage').click(function () {
        var items = prompt('Name des Pakets eingeben, Version und Timestamp wird automatisch hinzugefügt', 'Paketname')
        if (items != null) {
            let d = new Date()
            let timestamp = d.getFullYear() + ('0' + (d.getMonth() + 1)).slice(-2) + ('0' + d.getDate()).slice(-2) + ('0' + d.getHours()).slice(-2) + ('0' + d.getMinutes()).slice(-2)
            items = timestamp + items + 'V010'
        }
        var packageAssembly = { packageid: items, videos: [] }
        $.ajax({
            type: 'POST',
            url: REGIOTV_SERVER + '/package/assembly?packageid=' + packageAssembly.packageid,
            headers:{"Userlogin":userLogin},
            contentType: 'application/json',
            data: JSON.stringify(packageAssembly),
            success: function (data, status) {
                getPackages();
            },
            error: function (error) {
                console.log(error)
            }
        })
    })

    $('#removePackage').click(function () {
        const id = $('#regiotv-packages').find('.active').attr('id')
        $.ajax({
            type: 'DELETE',
            url: REGIOTV_SERVER + '/package?packageid=' + id,
            success: function () {
                getPackages();
            }
        })
    })

    $('#savePackage').click(function () {
        var packageAssembly = { name: $('.active').attr('id') }
        packageAssembly.videos = []
        $('#regiotv-package-content p').each(function (index, element) {
            packageAssembly.videos.push({ videoid: $(element).attr('id') })
        })
        $.ajax({
            type: 'POST',
            url: REGIOTV_SERVER + '/package/assembly?packageid=' + packageAssembly.name,
            headers:{"Userlogin":userLogin},
            contentType: 'application/json',
            data: JSON.stringify(packageAssembly),
            success: function () {
                $('#regiotv-editor-status p').html(`Paket  <b>${packageAssembly.name}</b> wurde erfolgreich aktualisiert.`)
                $('#regiotv-editor-status').removeClass('hidden')
            }
        })
    })

    $('#regiotv-editor-status button').click(function () {
        $('#regiotv-editor-status').addClass('hidden')
    })

    $('#videofilter').on('input' ,function(){
        $('#regiotv-videos p').removeClass('d-none');
        const filterRegex = RegExp($('#videofilter').val() , "i");
        $('#regiotv-videos p').each(function(index,element){
            if(!filterRegex.test($(element).text())){ 
                $(element).addClass('d-none');
            }
        })
    })
})
