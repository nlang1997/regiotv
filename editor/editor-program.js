jQuery(document).ready(($) => {
    // call to origin for obtaining the username of the logged
    // in user
    var userLogin;
    let packageListSave;
    $.ajax({
        type: 'GET',
        beforeSend: function () { },
        url: document.location,
        success: function (_,_,jqHXR) {
            userLogin = jqHXR.getResponseHeader('Userlogin');
            getPackages();
        },
    })

    //API call to get the program titles and display them in the program column
    var getPrograms = function(){
    $('#regiotv-programs').empty();
    $.ajax({
        type: 'GET',
        beforeSend: function () { },
        url: REGIOTV_SERVER + '/program/list',
        dataType:'json',
        success: function (data) {
            $.each(data, function (_, val) {
                let cssClass = 'list-group-item ui-state-default'
                var regex = new RegExp("^[0-9]*(.*)V010$")
                var matches = regex.exec(val);
                if(!matches){
                    matches = val;
                }
                $('#regiotv-programs').append(`<li title='${val}' class='${cssClass}' id='${val}'>${matches[1]}</li>`)
            })
        }
    })
    }
    getPrograms();
    
    //API call to get the program titles and display them in the program column
    let getPackages = function(){
    $.ajax({
        type: 'GET',
        beforeSend: function () { },
        dataType: 'json',
        url: REGIOTV_SERVER + '/package/list',
        success: function (packageList) {
            $.each(packageList, function (_, package) {
                let cssClass = 'list-group-item ui-state-default'
                var regex = new RegExp("^[0-9]*(.*)V010$")
                var matches = regex.exec(package);
                $('#regiotv-packages').prepend(`<p title='${package}' class='${cssClass}' id='${package}'>${matches[1]}</p>`)
            })
            packageListSave = $('#regiotv-packages p');
        } 
      })
    }

    $('#regiotv-program-content, #regiotv-packages').sortable({
        connectWith: '.connectedSortable'
    }).disableSelection()

    // show contained items on click
    $('#regiotv-programs').selectable({
        selected: function (_, ui) {
            $(ui.selected).addClass('active').siblings().removeClass('active')
            $('#regiotv-packages').empty().append(packageListSave);
            const id = $(ui.selected).attr('id')
            $.ajax({
                type: 'GET',
                url: REGIOTV_SERVER + '/program/assembly',
                data: {programid: id.toString()},
                dataType:'json',
                success: function (programAssembly) {
                    $.each(programAssembly.packages, function (_, val) {
                        $('#regiotv-packages p').each(function (_, element) {
                            if ($(element).attr('id') === val.packageid) {
                                $('#regiotv-program-content').append($(element))
                            }
                        })
                    })
                },
                complete: function () {
                    $('#regiotv-packege-content, #regiotv-packages').sortable('refresh')
                }
            })
        }
    })

    $('#addProgram').click(function () {
        var items = prompt('Name des Pakets eingeben, Version und Timestamp wird automatisch hinzugefügt', 'Paketname')
        if (items != null) {
            let d = new Date()
            let timestamp = d.getFullYear() + ('0' + (d.getMonth() + 1)).slice(-2) + ('0' + d.getDate()).slice(-2) + ('0' + d.getHours()).slice(-2) + ('0' + d.getMinutes()).slice(-2)
            items = timestamp + items + 'V010'
        }
        var programAssembly = { programid: items, programs: [] }
        $.ajax({
            type: 'POST',
            url: REGIOTV_SERVER + '/program/assembly?programid=' + programAssembly.programid,
            headers:{"Userlogin":userLogin},
            contentType: 'application/json',
            data: JSON.stringify(programAssembly),
            success: function (_, _) {
                getPrograms();
            },
            error: function (error) {
                console.log(error)
            }
        })
    })

    $('#removeProgram').click(function () {
        const id = $('#regiotv-programs').find('.active').attr('id')
        $.ajax({
            type: 'DELETE',
            url: REGIOTV_SERVER + '/program?programid=' + id,
            success: function () {
                getPrograms();
            }
        })
    })

    $('#saveProgram').click(function () {
        var programAssembly = { name: $('.active').attr('id') }
        programAssembly.programs = []
        $('#regiotv-program-content p').each(function (_, element) {
            programAssembly.programs.push({ programid: $(element).attr('id') })
        })
        $.ajax({
            type: 'POST',
            url: REGIOTV_SERVER + '/program/assembly?programid=' + programAssembly.name,
            headers:{"Userlogin":userLogin},
            contentType: 'application/json',
            data: JSON.stringify(programAssembly),
            success: function () {
                $('#regiotv-editor-status p').html(`Paket  <b>${programAssembly.name}</b> wurde erfolgreich aktualisiert.`)
                $('#regiotv-editor-status').removeClass('hidden')
            }
        })
    })

    $('#regiotv-editor-status button').click(function () {
        $('#regiotv-editor-status').addClass('hidden')
    })

    $('#packagefilter').on('input' ,function(){
        $('#regiotv-packages p').removeClass('d-none');
        const filterRegex = RegExp($('#packagefilter').val() , "i");
        $('#regiotv-packages p').each(function(_,element){
            if(!filterRegex.test($(element).text())){ 
                $(element).addClass('d-none');
            }
        })
    })
})
