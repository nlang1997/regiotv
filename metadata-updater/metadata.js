$(document).ready(function(){
    var server = REGIOTV_SERVER; 
    let userLogin;

    videojs.options.hls.overrideNative = true
    var player = videojs($('video-js').get(0), {
        inactivityTimeout: 0,
        controls:true,
        muted: false,
        autoplay: false,
        preload: "auto",
        fluid:true
    });
    $.ajax({
        type: 'GET',
        beforeSend: function () { },
        url: document.location,
        success: function (data,status,jqHXR) {
            userLogin = jqHXR.getResponseHeader('Userlogin');
    $.ajax(server+"/video/list",{
        method:"GET",
        dataType:"json",
        success:function(videos,textStatus){
            videos.forEach(function(element,index){
                var regex = new RegExp("^[0-9]*(.*)V010$")
                var matches = regex.exec(element);
                var videoentry = {};
                $.ajax({
                    url:`${REGIOTV_SERVER}/video/metadata?videoid=${element}`,
                    method:'GET',
                    dataType:'json',
                    success:function(metadata){
                        videoentry = $("<tr><td>" + metadata.title + "</td></tr>");
                        var releaseBtn = $("<button type='button' class='btn btn-success m-1 d-none' data-videoid="+element+">Freigeben</button>");	
                        var playBtn = $(`<button type='button' class='btn btn-success m-1' data-videoid="${element}" data-toggle="modal" data-target="#videojsModal">Abspielen</button>`);	
                        if(metadata.isReleased || metadata.owner == userLogin){
                            releaseBtn.prop("disabled",true);
                        }
                            playBtn.on('click',(event)=>{
                                $('#videojsModalLabel').text(metadata.title);
                                player.src({src:`${REGIOTV_SERVER}/video/masterplaylist?videoid=${$(event.currentTarget).attr('data-videoid')}`,type:'application/x-mpegURL'});
                            });
                        //modal window
                        var updateBtn = $("<button type='button' class='btn btn-primary m-1' data-videoid="+element+" data-toggle='modal' data-target='#updateMetaModal'>Update</button>");	
                        var deleteBtn = $("<button type='button' class='btn btn-danger m-1' data-videoid="+element+">Löschen</button>");	

                        releaseBtn.on('click',function(){
                            $.ajax({
                                url:`${REGIOTV_SERVER}/video/release?`+$.param({videoid:releaseBtn.attr('data-videoid')}),
                                headers:{"Userlogin":userLogin},
                                method:"POST",
                                success:alert("Video Freigegeben!"),
                            });
                        });

                        deleteBtn.on('click',function(){
                            if(confirm(`Wollen sie das Video mit der ID ${deleteBtn.attr('data-videoid')} wirklich löschen ?`)){  
                                $.ajax({
                                    url:`${REGIOTV_SERVER}/video?`+$.param({videoid:deleteBtn.attr('data-videoid')}),
                                    method:"DELETE",
                                    success:alert("video geloescht!"),
                                });
                            }
                        });

                        updateBtn.on('click',function(){
                            $('#updateMetaModal').attr('data-videoid',updateBtn.attr('data-videoid'));
                            $('.metainput').each(function(index,element){
                                $(element).val("");
                                $(element).prop("checked",false);
                            });
                            $.ajax(server+"/video/metadata?videoid="+$('#updateMetaModal').attr('data-videoid'),{
                                method:"GET",
                                dataType:"json",
                                success:function(metadata,textStatus){
                                    var metainput = $(".metainput");
                                    Object.keys(metadata).forEach(function(element,index){
                                        $.each(metainput,function(index,metainputelement){
                                            if(element == $(metainputelement).attr('data-name')){
                                                if($(metainputelement).is(':checkbox')){
                                                    $(metainputelement).prop('checked',metadata[element]);
                                                }else{
                                                    $(metainputelement).val(metadata[element]);
                                                }
                                            }
                                        });
                                    });
                                }
                            });
                        });
                        var btnCell = $("<td class='text-right'></td>");
                        btnCell.append(updateBtn);
                        btnCell.append(releaseBtn);
                        btnCell.append(playBtn);
                        btnCell.append(deleteBtn);
                        videoentry.append(btnCell);
                        $("#videolist").append(videoentry);
                    }
                });
            })
        }
    });
        },
    })
    $('#renderBtn').on('click',function(){
        $.ajax(server+"/video/rerender?videoid="+$('#updateMetaModal').attr('data-videoid'),{
            method:"POST",
	    success:console.log("Render erfolgreich")
        });
    })
    $('#updateMetaBtn').on('click',function(){
        var metadata = {};
        $('.metainput').each(function(index, element){
            if($(element).prop('type') == 'checkbox'){
                metadata[$(element).attr("data-name")] = $(element).prop("checked");
            }else{
                metadata[$(element).attr("data-name")] = $(element).val();
            }
        });
        $.ajax(server+"/video/metadata?videoid="+$('#updateMetaModal').attr('data-videoid'),{
            method:"POST",
            contentType:"application/json",
            dataType:"json",
            data:JSON.stringify(metadata),
        });
    });
    $('#videojsModal').on('hidden.bs.modal',()=>{
        player.pause();
    });
});
