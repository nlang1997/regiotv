let player;
let isFirstVideo = true;
let skipFirstVideo = function(){
	if(isFirstVideo){
		isFirstVideo=false;
		player.playlist.currentItem(player.playlist.next());
		$('video-js')[0].scrollIntoView();
	}
}
$(window).on('load',function(){
	$(".entry-title").addClass("d-none"); 
	let mediacenter = $('<div class="container-fluid"><div class="row pb-2"></div></div>');
	let categories = [
		'<button class="col" onclick="skipFirstVideo()" style="font-size:24px">Nachrichten</button>',
		'<button class="col" >Mediathek<br>Gemeinde</button>',
		'<button class="col" >Mediathek<br>Vereine</button>',
		'<button class="col" >Mediathek<br>Kultur</button>',
		'<button class="col" >Mediathek<br>Kirche</button>',
		'<button class="col" >Mediathek<br>Touristik</button>',
		'<button class="col" >Mediathek<br>Veranstaltungen</button>'
	];
	for(let category of categories){
		mediacenter.find('div').append($(category));
	}
	$(".content").prepend(mediacenter);
	videojs.options.hls.overrideNative = true
	player = videojs($('video-js').get(0), {
		inactivityTimeout: 0,
		controls:true,
		muted: false,
		autoplay: true,
		preload: "auto",
		fluid:true
	});
	var startprogram = "201812312359StartBiebertalV010";
	var test = $('#videojsplayer').attr('run');
	console.log(test);
	if(test){
		startprogram = test;
	}
	console.log(startprogram);
	var server = REGIOTV_SERVER;

	// Videocontrols
	var playerControls = $('<div id="playercontrols" class="py-1"></div>'); 
	playerControls.css('display','flex');
	playerControls.css('justify-content','space-between');
	var step_back = $('<button><i class="fas fa-step-backward"></i></button>');
	step_back.css('padding','0px 24px');
	step_back.css('height','35px');
	step_back.find('i').css("color","white");
	step_back.find('i').css("font-family","FontAwesome");
	step_back.find('i').css("font-style","normal");
	var currentVideo = $('<p class="my-auto" id="currentvideo"></p>');
	var step_forward = $('<button><i class="fas fa-step-forward"></i></button>');
	step_forward.find('i').css("color","white");
	step_forward.css('padding','0px 24px');
	step_forward.css('height','35px');
	step_forward.find('i').css("font-family","FontAwesome");
	step_forward.find('i').css("font-style","normal");
	$('.site-inner').css('max-width','80%');


	playerControls.append(step_back).append(currentVideo).append(step_forward);
	$('#videojs-controls').append(playerControls);

	var getPlaylist = function(programid){
		$.ajax({
			url:server+'/program/playlist?programid='+programid,
			success:function(playlist){
				var videolist = [];
				playlist.videos.forEach(function(element,index){
					videolist.push({sources:{src:server+element.src,type:element.type}});
				});
				player.playlist(videolist);
				player.playlist.autoadvance(0); player.playlist.repeat(true);
				step_forward.on('click',()=>{player.playlist.currentItem(player.playlist.next());isFirstVideo=false;});
				step_back.on('click',()=>{player.playlist.currentItem(player.playlist.previous())});
			},
			async:false
		},'json');
	};
	var metadata = [];
	var getMetadata = function(programid){
		$.ajax({
			url:server+'/program/metadata?programid='+programid,
			success:function(videometadata){
				metadata = JSON.parse(videometadata);
			},
			async:false
		},'json');
	};
	getPlaylist(startprogram);
	getMetadata(startprogram);
	player.on('playing',()=>{
		console.log(metadata[player.playlist.currentItem()].title);
		$('#currentvideo').text(metadata[player.playlist.currentItem()].title);
	});
});
