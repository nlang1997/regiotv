<?php
/*
Plugin Name: RegioTV Plugin
Plugin URI: -
Description: Features: Bearbeiten von Paketen, Hochladen von Texten/Videos
Author: RegioTainment
Version: 0.0
Author URI: https://biebertal.mach-mit.tv
Text Domain: regiotv
 */

defined('ABSPATH') or die;
define('regiotv_path', plugins_url() . '/' . dirname(plugin_basename(__FILE__)));

add_action('wp_enqueue_scripts', 'regiotv_scripts');
add_filter('wp_headers','add_user_header');

add_shortcode('regiotv-editor-package', 'regiotv_editor_package_show_page');
add_shortcode('regiotv-editor-program','regiotv_editor_program_show_page');
add_shortcode('regiotv-generator-video', 'regiotv_generatorVideo_show_page');
add_shortcode('regiotv-generator-message', 'regiotv_generatorMessage_show_page');
add_shortcode('regiotv-package-log', 'regiotv_package_log_show_page');
add_shortcode('regiotv-manage-templates', 'regiotv_manage_templates');
add_shortcode('regiotv-video-metadata', 'regiotv_video_metadata_show_page');
add_shortcode('regiotv-stream', 'regiotv_stream_show_page');
add_shortcode('regiotv-media-center', 'regiotv_media_center_show_page');

function regiotv_scripts()
{
    wp_register_script("regiotv_globals_js", regiotv_path . "/globals.js");
    wp_register_script("tus_js", regiotv_path . "/tus-js-client/tus.min.js");
    wp_register_script("bootstrap_js", "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js");
    wp_register_script("videojs_js", "https://vjs.zencdn.net/7.6.0/video.js");
    wp_register_script("playlist_js", regiotv_path . "/stream/playlist.js");

    wp_register_style("videojs", "https://vjs.zencdn.net/7.6.0/video-js.css");
    wp_register_style("bootstrap", "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css");
    wp_register_style("jquery-ui", "https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css");
    wp_register_style("fontawesome","https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
}

function add_user_header(){
    $current_user = wp_get_current_user();
    header('Userlogin:' . $current_user->user_login);
}

function regiotv_editor_package_show_page()
{
    wp_enqueue_style("regiotv_editor_css", regiotv_path . "/editor/editor.css", array("bootstrap"));
    wp_enqueue_script("regiotv_editor_package_js", regiotv_path . "/editor/editor-package.js", array("regiotv_globals_js", "jquery", "jquery-ui-core", "jquery-ui-selectable", "jquery-ui-sortable"));
    $context = array("ssl" => array(
        "verify_peer" => false,
        "verify_peer_name" => false,
    ));
    readfile(regiotv_path . "/editor/editor-package.html", false, stream_context_create($context));
}

function regiotv_editor_program_show_page()
{
    wp_enqueue_style("regiotv_editor_css", regiotv_path . "/editor/editor.css", array("bootstrap"));
    wp_enqueue_script("regiotv_editor_program_js", regiotv_path . "/editor/editor-program.js", array("regiotv_globals_js", "jquery", "jquery-ui-core", "jquery-ui-selectable", "jquery-ui-sortable"));
    $context = array("ssl" => array(
        "verify_peer" => false,
        "verify_peer_name" => false,
    ));
    readfile(regiotv_path . "/editor/editor-program.html", false, stream_context_create($context));
}

function regiotv_generatorVideo_show_page()
{
    wp_enqueue_style("regiotv_generator_css", regiotv_path . "/generator/generator.css", array("bootstrap"));
    wp_enqueue_script("regiotv_generator_js", regiotv_path . "/generator/generator.js", array("regiotv_globals_js", "tus_js", "jquery"));
    $context = array("ssl" => array(
        "verify_peer" => false,
        "verify_peer_name" => false,
    ));
    readfile(regiotv_path . "/generator/generator-video.html", false, stream_context_create($context));
}

function regiotv_manage_templates() {
  wp_enqueue_style("regiotv_template_generator_css", regiotv_path . "/template-generator/template-generator.css", array("bootstrap"));
  wp_enqueue_script("regiotv_template_generator_js", regiotv_path . "/template-generator/template-generator.js", array("regiotv_globals_js", "tus_js", "jquery"));
  $context = array("ssl" => array(
      "verify_peer" => false,
      "verify_peer_name" => false,
  ));

  include_once plugin_dir_path(__FILE__) . "/template-generator/template-generator.php";
}

function regiotv_generatorMessage_show_page()
{
    wp_enqueue_style("regiotv_generator_css", regiotv_path . "/generator/generator.css", array("bootstrap"));
    wp_enqueue_script("regiotv_generator_js", regiotv_path . "/generator/generator.js", array("regiotv_globals_js", "tus_js", "jquery"));
    $context = array("ssl" => array(
        "verify_peer" => false,
        "verify_peer_name" => false,
    ));
    //readfile(regiotv_path . "/generator/generator-message.html", false, stream_context_create($context));
    include_once plugin_dir_path(__FILE__) . "/generator/generator-message.php";
}

function regiotv_package_log_show_page() {
    wp_enqueue_style("regiotv_packagelog_css", regiotv_path . "/logger/packagelog.css", array("bootstrap"));
    wp_enqueue_script("regiotv_packagelog_js", regiotv_path . "/logger/packagelog.js", array("regiotv_globals_js", "jquery"));
    $context = array("ssl" => array(
        "verify_peer" => false,
        "verify_peer_name" => false,
    ));
    readfile(regiotv_path . "/logger/packagelog.html", false, stream_context_create($context));
}

function regiotv_video_metadata_show_page() {
    wp_enqueue_style("regiotv_video_metadata_css", regiotv_path . "/metadata-updater/metadata.css", array("videojs","bootstrap","fontawesome"));
    wp_enqueue_script("regiotv_video_metadata_js", regiotv_path . "/metadata-updater/metadata.js", array("videojs_js","regiotv_globals_js","jquery","bootstrap_js"));
    $context = array("ssl" => array(
        "verify_peer" => false,
        "verify_peer_name" => false,
    ));
    readfile(regiotv_path . "/metadata-updater/metadata.html", false, stream_context_create($context));
}

function regiotv_stream_show_page() {
    wp_enqueue_style("regiotv_stream_css", regiotv_path . "/stream/stream.css", array("bootstrap","fontawesome","videojs"));
    wp_enqueue_script("regiotv_stream_js", regiotv_path . "/stream/stream.js", array("videojs_js","playlist_js","jquery","regiotv_globals_js"));
    $context = array("ssl" => array(
        "verify_peer" => false,
        "verify_peer_name" => false,
    ));
    $player= "<video-js id='videojsplayer' class='video-js'></video-js>";
    stream_context_create($context);
    print($player);
}

function regiotv_media_center_show_page($atts) {
    wp_enqueue_style("regiotv_media_center_css", regiotv_path . "/media-center/media-center.css", array("bootstrap","fontawesome","videojs"));
    wp_enqueue_script("regiotv_media_center_js", regiotv_path . "/media-center/media-center.js", array("videojs_js","bootstrap_js","playlist_js","jquery","regiotv_globals_js"));
   $atts = shortcode_atts(['category' => 'none'],$atts);
    include("wp-content/plugins/regiotv/media-center/media-center.php");
}
