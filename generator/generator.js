jQuery(document).ready(($) => {
  let userLogin;
    $.ajax({
        type: 'GET',
        beforeSend: function () { },
        url: document.location,
        success: function (data,status,jqHXR) {
            userLogin = jqHXR.getResponseHeader('Userlogin');
        },
    })

    $('#creditfields').on('click', function (event) {
        $('.creditfields').toggleClass('hidden')
    })
    $('#vmsgmedia').on('click', function (event) {
        $('.vmsgmedia').toggleClass('hidden')
    })

    var upload = null
    var uploadIsRunning = false
    var input = $('#regiotv-generator-fileInput')
    var progressBar = $('.bar')
    var alertBox = document.querySelector('#support-alert')
    var uploadList = document.querySelector('#upload-list')
    if (!tus.isSupported) {
        alertBox.classList.remove('hidden')
    }

    /*if (!toggleBtn) {
        throw new Error('Toggle button not found on this page. Aborting upload-demo. ')
    }*/

	$('#regiotv-generator-uploadVideo').click(bootUpload)
	$("#regiotv-generator-uploadMessage").click(bootUpload)

    function bootUpload() {
        if (upload) {
            if (uploadIsRunning) {
                upload.abort()
                uploadIsRunning = false
            } else {
                upload.start()
                uploadIsRunning = true
            }
        } else {
            if (input[0].files.length > 0) {
                startUpload()
            } else {
                input.click()
            }
        }
    }

    function startUpload () {
        var file = input[0].files[0]
        // Only continue if a file has actually been selected.
        // IE will trigger a change event even if we reset the input element
        // using reset() and we do not want to blow up later.
        if (!file) {
		console.error("No file given for upload.")
            return
        }

        let data = readMetadata()
        if (!data) return
        data.filename = file.name
        data.filetype = file.type
	    data.version = "V010";

        var options = {
            endpoint: "http://localhost:8080/files/",
            resume: true,
            chunkSize: Infinity,
            retryDelays: [0, 1000, 3000, 5000],
            metadata: data,
            onError: function (error) {
                if (error.originalRequest) {
                    if (window.confirm('Failed because: ' + error + '\nDo you want to retry?')) {
                        upload.start()
                        uploadIsRunning = true
                        return
                    }
                } else {
                    window.alert('Failed because: ' + error)
                }

                reset()
            },
            onProgress: function (bytesUploaded, bytesTotal) {
        $('#regiotv-generator-status').css('background-color', 'yellow')
       	let isVideoMessage = $('*').hasClass('vmsginput');
        if(isVideoMessage){
	    	$('#regiotv-generator-status').text('Videonachricht erfolgreich hochgeladen: '+upload.url)
        }else{
	    	$('#regiotv-generator-status').text('Video erfolgreich hochgeladen: '+upload.url)
        }
        $('#regiotv-generator-status').text('Textnachricht wird hochgeladen...')
                var percentage = (bytesUploaded / bytesTotal * 100).toFixed(2)
                progressBar.css('width', percentage + '%')
                console.log(bytesUploaded, bytesTotal, percentage + '%')
            },
            onSuccess: function () {
		    $('#regiotv-generator-status').css('background-color', 'green')
        	    let isVideoMessage = $('*').hasClass('vmsginput');
		    if(isVideoMessage){
		    	$('#regiotv-generator-status').text('Videonachricht erfolgreich hochgeladen: '+upload.url)
		    }else{
		    	$('#regiotv-generator-status').text('Video erfolgreich hochgeladen: '+upload.url)
		    }

                reset()
            }
        }

        upload = new tus.Upload(file, options)
        upload.start()
        uploadIsRunning = true
    }

    function reset () {
        input.value = ''
        upload = null
        uploadIsRunning = false
    }

    function readMetadata () {
        let ok = true
        $('.required').each((i, e) => {
            if (e.value === '') {
                ok = false
                $('#regiotv-generator-status').css('background-color', 'red')
                $('#regiotv-generator-status').text('Bitte füllen Sie alle mit * markierten Felder aus!')
            }
        })
        if (!ok) {
            return false
        }
        let data = {owner:userLogin}
        let isVideoMessage = $('*').hasClass('vmsginput');
        if(isVideoMessage){
            data.videomessage = true;
            $('.vmsginput').each(function (index, element) {
                if ($(element).hasClass('split')) {
            let values = $(element).val().split(';')
            values.map((s) => s.trim())
            data[$(element).attr('name')] = values
                } else {
            data[$(element).attr('name')] = $(element).val()
                };
            })
            return data
        }else{
            $('.vuplinput').each(function (index, element) {
                if ($(element).hasClass('split')) {
            let values = $(element).val().split(';')
            values.map((s) => s.trim())
            data[$(element).attr('name')] = values
                } else {
            data[$(element).attr('name')] = $(element).val()
                };
            })
            return data
        }
    }
})	// JQuery ready
