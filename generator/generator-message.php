<div class="container">
        <label class="row">Designvorlage
          <div class="templates">
        <?php
          foreach ($templates as $template) {
            echo '<div class="templateContainer">';
              echo '<img src="'. $template->background .'"/>';
              echo '<p>' .$template->title  . '</p>';
            echo '</div>';
          }
          ?>
        </div>
        </label>
        <label class="row">Titel *
            <input type="text" class="vmsginput required" name="title" placeholder="Vollständiger Titel des generierten Videos"
                >
        </label>

        <label class="row">Untertitel *
            <input type="text" class="vmsginput required" name="subtitle" placeholder="Untergeordneter Titel mit zusätzlichen Informationen">
        </label>

        <label class="row">Kurzname *
            <input type="text" class="vmsginput required" name="shortname" placeholder="Kurztitel (keine Leerzeichen und Umlaute erlaubt!)"
                >
        </label>

        <label class="row">Beschreibung
            <textarea class="vmsginput" name="description" placeholder="Ausführliche Beschreibung des Videos"></textarea>
        </label>

        <label class="row">Inhalt *
            <textarea class="vmsginput required" name="content" placeholder="Der Inhalt der Textnachricht"></textarea>
        </label>

        <label class="row">Autoren *
            <input type="text" class="vmsginput required split" name="authors" placeholder="Durch Semikolon (;) getrennte Namen der Autoren"
                >
        </label>

        <label class="row">Genre
            <input type="text" class="vmsginput" name="genre" placeholder="Genre des Videos (Kultur, Sport, ...)">
        </label>

        <label class="row">Tags
            <input type="text" class="vmsginput split" name="tags" placeholder="Durch Semikolon (;) getrennte Schlagworte">
        </label>

            <label class="row">Hintergrundbild *</label>
            <input type="file" class="picture vmsginput required" id="regiotv-generator-fileInput">

        <button id="creditfields" class="hidden"> Abspann bearbeiten</button>
          <div class="creditfields hidden">
              <label class="row">Interview
                <input type="text" class="vmsginput split" name="interviews" placeholder="Durch Semikolon (;) getrennte Namen der Interviewten" >
              </label>
                <label class="row">Statement
                  <input type="text" class="vmsginput split" name="statements" placeholder="Durch Semikolon (;) getrennte Namen der Statementgeber " >
                </label>
                  <label class="row">Sprecher
                    <input type="text" class="vmsginput split" name="speakers" placeholder="Durch Semikolon (;) getrennte Namen der Sprecher " >
                  </label>
                    <label class="row">Musik
                      <input type="text" class="vmsginput split" name="music" placeholder="Durch Semikolon (;) getrennte Liste der Musik" >
                    </label>
                      <label class="row">Copyright
                        <input type="text" class="vmsginput split" name="copyright" placeholder="Durch Semikolon (;) Copyright information" >
                      </label>
          </div>

        <div class="row">
		<button id="regiotv-generator-uploadMessage" class="col-4">Hochladen</button>
		<span class="col-1"></span>
		<div class="col" id="regiotv-generator-status"></div>
	</div>
</div>
