$(()=>{
    $.ajax(REGIOTV_SERVER + "/package/log",{
        type:"GET",
        dataType:'json',
        error:function (err){
            console.log(err)
        },
        success:function(assemblylogs){
            assemblylogs.sort((a,b)=>{return a.time - b.time;});
            for(var entry of assemblylogs){
                appendLogRow(entry);
            }
        }
    });
    function appendLogRow(logentry){
        $('tbody').append($(`
            <tr>
                <td>${new Date(logentry.time).toLocaleString()}</td>
                <td>${logentry.packageid}</td>
                <td>${logentry.action}</td>
                <td>${logentry.video}</td>
                <td>${logentry.user}</td>
            </tr>`));
    }
});
